import React from "react";
import history from '../../router/history'
import './header.css'


class Header extends React.Component {
    logout () {
        console.log(history)
            localStorage.removeItem("username");
            // console.log(this.props)
            history.push("/login");
    };

    goBack () {
        history.goBack()
    }

    render() {
        return (
            <div className="header">
                <button onClick={this.goBack}>Back</button>
                <button onClick={this.logout}>Logout</button>
            </div>
        );
    }
}
export default Header;
