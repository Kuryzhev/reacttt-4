import React from 'react';
import { render } from 'enzyme';
import Header from "./header";
import Login from "../login/login";

describe('<Header />', () => {
    it('renders three `.lheader', () => {
        const wrapper = render(<Header />);
        expect(wrapper.find(<div className="header" />)).to.have.length(1);
    });

    it('renders three `.lheader', () => {
        const wrapper = render(<Header />);
        wrapper.children('button')[1].simulate('click');
        expect(props.navigation.navigate).toHaveBeenCalledWith(Login);
    });
});
