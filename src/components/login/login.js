import React from "react";
import './login.css'

class Login extends React.Component {
  state = { username: "" };

  handleSubmit = event => {
    event.preventDefault();
    if (this.state.username) {
      localStorage.setItem("username", this.state.username);
      this.props.history.push("/userList");
    }
  };

  setUsername(username) {
    this.setState(state => {
      return { username };
    });
  }
  render() {
    return (
        <div><h3 className="title">Login</h3>
            <div className="login-container">
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className="login-section">
                        <input
                            placeholder="User name"
                            type="text"
                            name="username"
                            onChange={e => this.setUsername(e.target.value)}
                        />
                    </div>
                    <button className="confirm-button" type="submit">
                        Sign In
                    </button>
                </form>
            </div>
        </div>
    );
  }
}
export default Login;
