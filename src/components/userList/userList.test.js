import React from 'react';
import { render } from 'enzyme';
import UserList from "./userList";
import UserDetails from "../userDetails/userDetails";

describe('<UserList />', () => {
    it('renders three `.list-container', () => {
        const wrapper = render(<UserList />);
        expect(wrapper.find(<div className="list-container" />)).to.have.lengthAdjust(1);
    });

    it('navigate after click to USerDetails', () => {
        const wrapper = render(<UserList title="unique" />);
        wrapper.find('list-container').simulate('click');
        expect(props.navigation.navigate).toHaveBeenCalledWith(UserDetails);
    });
});
