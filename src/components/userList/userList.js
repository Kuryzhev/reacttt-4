import React from "react";
import { getUserList } from "../../services/userService";
import "./userList.css";

class UserList extends React.Component {
  state = { users: [] };
  componentDidMount() {
    getUserList()
      .then(tasksResponse => {
        this.setState({
          users: tasksResponse.data
        });
      })
      .catch(error => {
        console.log(error);
      });
  }
  render() {
    return (
      <div className="container">
          <h3>User List</h3>
        <ul>
          {this.state.users.map(user => (
            <div className="list-container" key={user.id} onClick={() => this.props.history.push(`/userDetails/${user.id}`)}>
              <div className="comment">{user.title}</div>
              <div className={user.completed ? "active" : "disabled"}>{user.completed ? 'ACTIVE' : 'BLOCKED'}</div>
            </div>
          ))}
        </ul>
      </div>
    );
  }
}
export default UserList;
