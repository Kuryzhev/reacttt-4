import React from 'react';
import {getUserDetails} from "../../services/userService";
import './userDetails.css'

class UserDetails extends React.Component {
    state = { user: {} };

    componentDidMount() {
        getUserDetails(this.props.match.params.id).then(tasksResponse => {
                console.log(tasksResponse.data);
            this.setState({
                user: tasksResponse.data
            });
            })
            .catch(error => {
                console.log(error);
            });
    }
    render() {
        return (
            <div className="detail-container">
                    <div>
                        <p>Title: {this.state.user.title}</p>
                        <p>User ID: {this.state.user.userId}</p>
                        <p>ID: {this.state.user.id}</p>
                        <p className={this.state.user.completed ? "completed" : "not-completed"}>
                            Status: {this.state.user.completed ? "Completed" : "Not completed"}
                        </p>
                    </div>
            </div>
        );
    }
}
export default UserDetails;
