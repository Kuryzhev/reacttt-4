import React from "react";
import { Route, Redirect } from "react-router-dom";

export const PrivateRoute = ({component: Component, ...rest}) => {
    const isAuth = localStorage.getItem('username');
    return (
        <Route {...rest} render={props => (
            isAuth ?
                <Component {...props} />
                : <Redirect to={{pathname: "/login", state: {from: props.location}}}/>
        )}
        />
    );
};
