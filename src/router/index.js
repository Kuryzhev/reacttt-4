import React from "react";
import { Router, Switch, Redirect, Route } from "react-router-dom";
import {withRouter} from 'react-router';
import {PrivateRoute} from "./private";
import history from "./history"
import UserList from "../components/userList/userList";
import Login from "../components/login/login";
import UserDetails from "../components/userDetails/userDetails";
import Header from "../components/header/header";

export const AppRoute = () => {
    return (
        <Router history={history}>
            <div>
                <Header component={Header}r/>
                <Switch>
                    <Route path="/login" component={Login}/>
                    <PrivateRoute path="/userList" component={UserList}/>
                    <PrivateRoute path="/userDetails/:id" component={UserDetails}/>
                    <Redirect from='/' to='/userList'/>
                </Switch>
            </div>
        </Router>
    );
};
export default withRouter(AppRoute);
