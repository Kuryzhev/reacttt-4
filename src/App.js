import React from 'react';
import { Provider } from "react-redux";

import { AppRoute } from "./router/index";
import {combineReducers, createStore} from 'redux'

const store = createStore(combineReducers, ['user'])

const App = () => {
  return (
      <Provider store={store}>
        <AppRoute/>
      </Provider>
  );
};
export default App;
