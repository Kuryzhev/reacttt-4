import axios from "axios";
const serverUrl = 'https://jsonplaceholder.typicode.com'
export function getUserList() {
    return axios
        .get(`${serverUrl}/todos`)
}

export function getUserDetails(id) {
    return axios
        .get(`${serverUrl}/todos/${id}`)
}
